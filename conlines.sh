#!/bin/bash

CONFIG_FILE="$HOME/Git/Manage configuration lines/output"

var1='var1-línea'
#./inslines.sh "${var1}" "${CONFIG_FILE}"
bash ./inslines.sh "${var1}" "${CONFIG_FILE}"

declare -a vector1=(
    'vector1-línea1'
    'vector1-línea2'
    'vector1-línea3'
    'vector1-línea4'
)
#./inslines.sh "${vector1[@]}" "${CONFIG_FILE}"
bash ./inslines.sh "${vector1[@]}" "${CONFIG_FILE}"

declare -a vector2=(
    'vector2-línea1'
    'vector2-línea2'
    'vector2-línea3'
)
#./inslines.sh "${vector2[@]}" "${CONFIG_FILE}"
bash ./inslines.sh "${vector2[@]}" "${CONFIG_FILE}"

