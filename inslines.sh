#!/bin/bash

# Obtengo parámetros de entrada.
declare -a INPUT_PARAMETERS=("$@")
#echo "Número de parámetros de entrada: ${#INPUT_PARAMETERS[@]}"

# Obtengo fichero de configuración.
readonly CONFIG_FILE=${INPUT_PARAMETERS[-1]}
#echo "Fichero de configuración: ${CONFIG_FILE}"

# Obtengo líneas de configuración.
unset INPUT_PARAMETERS[-1]
declare -ra CONFIG_LINES=("${INPUT_PARAMETERS[@]}")
#echo "Líneas de configuración: ${CONFIG_LINES[@]}"


# Verificar permisos de ejecución.
check_execution_permissions (){

    if (( $EUID == 0 )); then

        echo "Debe ejecutar el script sin permisos root"
        exit -1
    fi
}


# Imprime mensaje si el fichero ha sido creado correctamente o no.
# Receive (String $configFile): Fichero de configuración.
# Receive (String $detail): Detalla si el fichero va a ser creado o existe previamente.
msg_create_file (){

    local -r configFile="$1"
    local -r detail="$2"

    if [ -f "${configFile}" ]; then

        echo -e "\n\e[32;1m::\e[37m Fichero '\e[32m${configFile}\e[37m' ${detail}.\e[0m"
    else
        echo -e "\n\e[31;1mError:\e[37m No ha sido posible crear fichero '\e[32m${configFile}\e[37m'.\e[0m"
        exit -1
    fi
}


# Validar y crea fichero de configuración.
# Receive (String $configFile): Fichero de configuración.
validate_config_file (){

    local -r configFile="$1"

    # Si el fichero de configuración no existe.
    if [ ! -f "${configFile}" ]; then

        echo "Fichero \"${configFile}\" no existe, creándolo..."

        # Si no es una ruta del espacio del usuario.
        if [[ ! $(echo "${configFile}" | egrep -o "^/home") ]]; then
            
            # Creo fichero con permisos root.
            sudo touch "${configFile}"
            msg_create_file "${configFile}" 'creado correctamente'
        else
            # Creo fichero.
            touch "${configFile}"
            msg_create_file "${configFile}" 'creado correctamente'
        fi
    else
        msg_create_file "${configFile}" 'ya existe'
    fi
}


# Validar líneas de configuración.
# Receive (String $actualLine): Línea actual.
# Receive (String $PreviusLine): Línea previa (opcional).
# Receive (String $configFile): Fichero de configuración.
validate_lines (){

    declare -i numParameters=$#
    local actualLine="$1"

    # Si el número de parámetros es igual a 3.
    if (( numParameters == 3 )); then

        local previusLine="$2"
        local configFile="$3"

        # Validar si $previusLine está vacía.
        if [[ "${previusLine}" == "" ]]; then

            echo "Línea actual: ${actualLine}"
        else
            echo "Línea actual: ${actualLine}"
            echo "Línea previa: ${previusLine}"

            # Validar si $previusLine existe en el fichero de configuración.
            # Si la línea actual existe en el fichero de configuración.
        fi
    # Si el número de parámetros es igual a 2.
    elif (( numParameters == 2 )); then

        #echo "validate_lines () contiene ${numParameters} parámetros ."
        local configFile="$2"

        # configLines[@] solo contiene una línea, por tanto no existe línea previa.
        echo "Línea actual: ${actualLine}"
    else
        echo "validate_lines () requiere 2 o 3 parámetros, no ${numParameters}."
    fi
}


# Envía $actualLine y $previusLine (si existe) a validate_lines ().
# Receive (String[] $configLines[@]): Array de líneas de configuración.
# Receive (String   $configFile): Fichero de configuración.
send_lines_to_validate (){
    
    declare -i numParameters=$#

    # Si el número de parámetros es igual a 2.
    if (( numParameters == 2 )); then

        #declare -ar cofigLines=("$1")
        local -nr configLines="$1"
        local -r configFile="$2"

        declare -i configLinesLenght=${#configLines[@]}
        local actualLine=''
        local previusLine=''

        #echo -e "configLines: ${configLines[@]} \nconfigLinesLenght: ${configLinesLenght} \nconfigFile: ${configFile}"
        #echo -e "\nconfigFile: ${configFile}"

        # Si $configLines[] contiene más de una línea.
        if (( configLinesLenght > 1 )); then

            for (( i=0; i<${configLinesLenght}; i++ ));
            do
                actualLine=${configLines[$i]}

                # Si la línea actual es distinta de la primera de configLines[].
                if (( i > 0 )); then

                    # Gestionar líneas apartir de la segunda, de configLines[].
                    previusLine="${configLines[(($i-1))]}"
                fi    

                # Valido líneas de configuración.
                validate_lines "${actualLine}" "${previusLine}" "${configFile}"
                #echo -e "Actual: ${actualLine} \nPrevia: ${previusLine}"
            done
        else
            actualLine="${configLines[@]}"

            # Valido una línea de configuración.
            validate_lines "${actualLine}" "${configFile}"
            #echo -e "Actual: ${actualLine}"
        fi
    else
        echo "send_lines_validate () requiere 2 parámetros, no ${numParameters}."
    fi
}


# Verificao permisos de ejecución.
check_execution_permissions

# Valido y creo fichero de configuración.
validate_config_file "${CONFIG_FILE}"

# Envío $actualLine y $previusLine (si existe) a validate_lines ().
send_lines_to_validate CONFIG_LINES "${CONFIG_FILE}"
